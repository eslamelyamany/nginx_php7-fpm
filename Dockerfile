FROM alpine:3.8

RUN apk --no-cache add \
	supervisor \
	nginx \
	php-fpm \
    php7 \
    php7-ctype \
    php7-curl \
    php7-imap \
    php7-dom \
    php7-fileinfo \
    php7-ftp \
    php7-iconv \
    php7-json \
    php7-mbstring \
    php7-mysqlnd \
    php7-openssl \
    php7-pdo \
	php7-pdo_mysql \
	php7-mysqli \
    php7-pdo_sqlite \
    php7-pear \
    php7-phar \
    php7-posix \
    php7-session \
    php7-simplexml \
    php7-sqlite3 \
    php7-tokenizer \
    php7-xml \
    php7-xmlreader \
    php7-xmlwriter \
    php7-zlib \
	php7-gd 

ENV nginx_vhosts /etc/nginx/sites-enabled
ENV php_conf /etc/php7/php.ini
ENV nginx_conf /etc/nginx/nginx.conf
ENV supervisor_conf /etc/supervisor/supervisord.conf

COPY supervisord.conf ${supervisor_conf}

RUN set -x 
#RUN addgroup -g 82 -S www-data 
RUN adduser -u 82 -D -S -G www-data www-data
RUN mkdir -p /run/php /var/www/html /run/nginx
RUN chown -R www-data:www-data /var/www/html
RUN chown -R www-data:www-data /run/php


RUN sed -i -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g; \
    s/128M/512M/g; \
    s!;date.timezone \=!date.timezone \= \"Asia/Riyadh\"!g' ${php_conf} 
RUN echo "daemon off;" >> ${nginx_conf}
RUN echo "include ${nginx_vhosts}/*.conf;" > /etc/nginx/conf.d/default.conf
RUN sed -i "s/^listen.*9000$/listen = \/run\/php\/php7.0-fpm.sock\nlisten.owner = www-data\nlisten.group = www-data/g; \
    s/nobody/www-data/g" /etc/php7/php-fpm.d/www.conf
RUN sed -i 's/user nginx/user www-data/g' ${nginx_conf}
COPY ioncube/ioncube_loader_lin_7.2.so /usr/lib/php7/modules/
COPY ioncube/00_ioncube.ini /etc/php7/conf.d/
VOLUME ["/etc/nginx/sites-enabled", "/etc/crontabs/", "/var/log/nginx", "/var/www/html", "/etc/php7"]

COPY start.sh /start.sh
RUN chmod +x /start.sh

CMD ["./start.sh"]

EXPOSE 80 443