# CONTENT #

This package includes the following :

nginx : 1.14.0

php : 7.2.8 with php-fpm

php modules : 
Core
ctype
curl
date
dom
fileinfo
filter
ftp
gd
hash
iconv
ionCube Loader
json
libxml
mbstring
mysqli
mysqlnd
openssl
pcre
PDO
pdo_mysql
pdo_sqlite
Phar
posix
readline
Reflection
session
SimpleXML
SPL
sqlite3
standard
tokenizer
xml
xmlreader
xmlwriter
zlib


### INSTALL ###

docker pull eslamelyamany/nginx_php7-fpm:latest

### RUN ###

docker run -d -p 443:443 -p 80:80 eslamelyamany/nginx_php7-fpm

### OPTIONS ###

ports :
  - 80 (http)
  - 443 (https)
  
 volumes :
   - /etc/nginx/sites-enabled (nginx vhosts)
   - /etc/nginx/certs (for nginx ssl certificates)
   - /var/log/nginx (nginx logs)
   - /var/www/html (for files)

### Notes ###

  - for files make sure to change owner of files to UID 82