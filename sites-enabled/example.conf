server {
	listen   80; ## listen for ipv4; this line is default and implied
	listen              443 ssl;

        server_name example.com;

	root /var/www/html;

    	ssl_certificate     certs/example.com.crt;
    	ssl_certificate_key certs/example.com.key;
        ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers         HIGH:!aNULL:!MD5;

	index index.php index.htm index.html;
	# Add stdout logging
	error_log /var/log/nginx/error.log debug;
	access_log /var/log/nginx/access.log;

        location / {
                # This is cool because no php is touched for static content.
                # include the "?$args" part so non-default permalinks doesn't break when using query string
                try_files $uri $uri/ /index.php?$args;
        }

	location ~ \.php$ {
                try_files $uri =404;
		fastcgi_split_path_info ^(.+\.php)(/.+)$;
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
		fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
    		fastcgi_param SCRIPT_NAME $fastcgi_script_name;
		fastcgi_index index.php;
		include fastcgi_params;
	}

        location ~* \.(jpg|jpeg|gif|png|css|js|ico|webp|tiff|ttf|svg)$ {
                expires           5d;
        }

	# deny access to . files, for security
	#
	location ~ /\. {
    		log_not_found on; 
    		deny all;
	}
        
	location ^~ /.well-known {
                allow all;
                auth_basic off;
        }

}
